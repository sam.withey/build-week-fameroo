WITH centeralised_stall_order_receipt AS (
    SELECT o.order_id,
           o.order_date,
           m.name                                           AS market_name,
           o.total_amount,
           concat(driver.first_name, ' ', driver.last_name) AS driver_name,
           concat(picker.first_name, ' ', picker.last_name) AS picker_name,
           payment.payment_date,
           payment.approved                                 AS payment_approved,
           concat(usr.first_name, ' ', usr.last_name)       AS customer_name,
           usr.address                                      AS customer_address,
           usr.postcode                                     AS customer_postcode,
           usr.phone_number                                 AS customer_phone_number
    FROM fameroo_postgres_demo.order AS o
             LEFT JOIN fameroo_postgres_demo.driver driver on o.driver_id = driver.driver_id
             LEFT JOIN fameroo_postgres_demo.driver picker ON o.picker_id = picker.driver_id
             LEFT JOIN fameroo_postgres_demo.payment payment on o.payment_id = payment.payment_id
             LEFT JOIN fameroo_postgres_demo."user" usr on o.user_id = usr.user_id
             LEFT JOIN fameroo_postgres_demo.market m on o.market_id = m.market_id
)

SELECT *
FROM centeralised_stall_order_receipt;
