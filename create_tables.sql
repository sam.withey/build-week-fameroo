CREATE SCHEMA IF NOT EXISTS fameroo_postgres_demo;

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo.market
(
    market_id integer not null
        constraint market_pk
            primary key,
    name      text,
    address   text,
    rating    double precision,
    open      boolean
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo.stall
(
    stall_id    integer not null
        constraint stall_pk
            primary key,
    name        text,
    description text,
    open        boolean,
    twitter     text,
    website     text,
    facebook    text,
    instagram   text,
    market_id   integer
        constraint stall_market_market_id_fk
            references fameroo_postgres_demo.market
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo.product
(
    product_id          serial
        constraint product_pk
            primary key,
    sku_code            integer,
    product_name        text,
    product_description text,
    price               double precision,
    allergy_info        text,
    available           boolean,
    stall_id            integer
        constraint product_stall_stall_id_fk
            references fameroo_postgres_demo.stall
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo."user"
(
    user_id      integer not null
        constraint user_pk
            primary key,
    first_name   text,
    last_name    text,
    phone_number text,
    dob          timestamp,
    address      text,
    postcode     text,
    country_code text,
    country      text
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo.driver
(
    driver_id  integer not null
        constraint driver_pk
            primary key,
    first_name text,
    last_name  text,
    rating     double precision
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo.payment
(
    payment_id     integer not null
        constraint payment_pk
            primary key,
    order_id       integer,
    payment_date   timestamp,
    payment_amount double precision,
    approved       boolean
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo."order"
(
    order_id     integer not null
        constraint order_pk
            primary key,
    user_id      integer
        constraint order_user_user_id_fk
            references fameroo_postgres_demo."user",
    driver_id    integer
        constraint order_driver_driver_id_fk
            references fameroo_postgres_demo.driver,
    picker_id    integer
        constraint order_driver_driver_id_fk_2
            references fameroo_postgres_demo.driver,
    order_date   timestamp,
    comments     text,
    total_amount double precision,
    order_fee    double precision,
    delivery_fee double precision,
    tips         double precision,
    picker_fee   integer,
    payment_id   integer
        constraint order_payment_payment_id_fk
            references fameroo_postgres_demo.payment,
    market_id    integer
        constraint order_market_market_id_fk
            references fameroo_postgres_demo.market
);

CREATE TABLE IF NOT EXISTS fameroo_postgres_demo.product_order
(
    product_order_id integer not null
        constraint product_order_pk
            primary key,
    order_id         integer
        constraint product_order_order_order_id_fk
            references fameroo_postgres_demo."order",
    product_id       integer
        constraint product_order_product_product_id_fk
            references fameroo_postgres_demo.product,
    total_amount     double precision,
    quantity         integer
);
