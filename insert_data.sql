insert into fameroo_postgres_demo.market (market_id, name, address, rating, "open")
values (1, 'Meantime Market Greenwich', 'Old Royal Naval College, College Way (off King William Walk), Greenwich, SE10 9NN', 4.4, True);

insert into fameroo_postgres_demo.stall (stall_id, name, description, "open", twitter, website, facebook, instagram, market_id)
values (1, 'Celtic Bakers', 'The Celtic Bakers is an artisan, wholesale bakery in North London, situated in the old Chocolate Factory.', True, '', '', '', '', 1),
       (2, 'Ebby’s Kitchen', 'Welcome to Ebby’s Kitchen, home to the awesome #Köfte & Halloumi Wrap', True, 'twitter.com/ebbyskitchen', '', 'facebook.com/ebbyskitchen', 'instagram.com/ebbyskitchen', 1);

insert into fameroo_postgres_demo.product (product_id, sku_code, product_name, product_description, price, allergy_info, available, stall_id)
values (1, '21341', 'croissant', 'almost crossiant', 1.80, 'Contains nuts', True, 1),
       (2,'24252', 'mince pie', 'Our best ever deep filled mince pie!', 1, 'Contains dairy and gluten', False, 1),
       (3, '24257', 'Kofte', 'Ground meat skewers', 7, 'Contains meat', True, 2),
       (4, '654354', 'Halloumi Wrap', 'Ebbys homemade halloumi wrap', 6.50, 'Contains dairy and gluten', True, 2);


insert into fameroo_postgres_demo."user" (user_id, first_name, last_name, phone_number, dob, address, postcode, country_code, country)
values (1, 'Samuel', 'Withey', '0778082324', '1996-08-12', 'Tenzing House, 18 Henrietta Street, London', 'WC2E 8QH', 'GB', 'England'),
       (2, 'Astara', 'Cambata', '0778083519', '1996-02-9', 'Tenzing House, 18 Henrietta Street, London', 'WC2E 8QH', 'GB', 'England');

insert into fameroo_postgres_demo.driver (driver_id, first_name, last_name, rating)
values (1, 'Ineta', 'Bliudziute', 4.7),
       (2, 'Matthew', 'Horne', 3.9);

insert into fameroo_postgres_demo.payment (payment_id, order_id, payment_date, payment_amount, approved)
values (1, 1, '2021-12-15 13:19:02', 18.58, True),
       (2, 2, '2021-12-16 13:19:02', 38.50, True);


insert into fameroo_postgres_demo."order" (order_id, user_id, driver_id, picker_id, order_date, comments, total_amount, order_fee,
                     delivery_fee, tips, picker_fee, payment_id, market_id)
values (1, 1, 1, 2, '2021-12-15 13:19:02', 'The flakiest pastries please', 18.58, 0.49, 2.99, 2, 2, 1, 1),
       (2, 2, 2, 1, '2021-12-16 13:19:02', 'Love the market feel and vibe', 38.50, 0, 2.50, 0, 3, 2, 1);


insert into fameroo_postgres_demo.product_order (product_order_id, order_id, product_id, total_amount, quantity)
values (1, 1, 1, 3.60, 2),
       (2, 1, 4, 6.50, 1),
       (3, 2, 2, 6, 6),
       (4, 2, 3, 14, 2),
       (5, 2, 4, 13, 2);