WITH stall_eod_payments AS (
    SELECT s.stall_id,
           o.order_date,
           SUM(price * quantity) AS daily_total_order_value
    FROM fameroo_postgres_demo."order" AS o
             INNER JOIN fameroo_postgres_demo.product_order po on o.order_id = po.order_id
             INNER JOIN fameroo_postgres_demo.product p on po.product_id = p.product_id
             INNER JOIN fameroo_postgres_demo.stall s on p.stall_id = s.stall_id
    GROUP BY s.stall_id, order_date
    ORDER BY s.stall_id ASC, o.order_date ASC
)

SELECT *
FROM stall_eod_payments