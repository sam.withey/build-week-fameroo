WITH farmer_stall_product_receipt AS (
    SELECT o.order_id,
           o.order_date,
           s.name                                 AS stall_name,
           m.name                                 AS market_name,
           p.product_name                         AS product_name,
           po.quantity,
           concat(d.first_name, ' ', d.last_name) AS picker_name
    FROM fameroo_postgres_demo."order" AS o
             INNER JOIN fameroo_postgres_demo.product_order po on o.order_id = po.order_id
             INNER JOIN fameroo_postgres_demo.product p on po.product_id = p.product_id
             LEFT JOIN fameroo_postgres_demo.stall s ON p.stall_id = s.stall_id
             LEFT JOIN fameroo_postgres_demo.market m on s.market_id = m.market_id
             LEFT JOIN fameroo_postgres_demo.driver d on o.picker_id = d.driver_id
)

SELECT *
FROM farmer_stall_product_receipt
